package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.empleado;

public interface IEmpleadoService {

	public List<empleado> listarEmpleados();
	
	public empleado guardarEmpleado(empleado empleado);
	
	public empleado empleadoID(String id);

	public empleado actualizarEmpleado(empleado empleado);
	
	public void eliminarEmpleado(String id);
}
