package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.departamento;

public interface IDepartamentoService {

	public List<departamento> listarDepartamentos();
	
	public departamento guardarDepartamento(departamento departamento);
	
	public departamento departamentoID(Integer id);
	
	public departamento actualizarDepartamento(departamento departamento);
	
	public void eliminarDepartamento(Integer id);
}
