package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.departamento;

public interface IDepartamentoDAO extends JpaRepository<departamento, Integer>{

}
