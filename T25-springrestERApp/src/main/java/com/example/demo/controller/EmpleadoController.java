package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.empleado;
import com.example.demo.service.empleadoServiceImpl;

@RestController
@RequestMapping("/api")
public class EmpleadoController {

	@Autowired
	empleadoServiceImpl empleadoServiceImpl;
	
	@GetMapping("/empleados")
	public List<empleado> listarEmpleados(){
		return empleadoServiceImpl.listarEmpleados();
	}
	
	@PostMapping("/empleados")
	public empleado guardarEmpleado(@RequestBody empleado empleado) {
		return empleadoServiceImpl.guardarEmpleado(empleado);
	}
	
	@GetMapping("/empleados/{id}")
	public empleado empleadoID(@PathVariable(name = "id") String id) {
		
		empleado empleado_ID = new empleado();
		
		empleado_ID = empleadoServiceImpl.empleadoID(id);
		
		System.out.println("Empleado ID: " + empleado_ID);
		
		return empleado_ID;
	}
	
	@PutMapping("/empleados/{id}")
	public empleado actualizarEmpleado(@PathVariable(name = "id") String id, @RequestBody empleado empleado) {
		
		empleado empleado_sel = new empleado();
		empleado empleado_act = new empleado();
		
		empleado_sel = empleadoServiceImpl.empleadoID(id);
		
		empleado_sel.setNombre(empleado.getNombre());
		empleado_sel.setApellidos(empleado.getApellidos());
		empleado_sel.setDepartamento(empleado.getDepartamento());
		
		empleado_act = empleadoServiceImpl.actualizarEmpleado(empleado_sel);
		
		System.out.println("El empleado actualizado es: " + empleado_act);
		
		return empleado_act;
		
	}
	
	@DeleteMapping("/empleados/{id}")
	public void eliminarEmpleado(@PathVariable(name = "id") String id) {
		empleadoServiceImpl.eliminarEmpleado(id);
	}
}
