package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T25SpringrestErAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(T25SpringrestErAppApplication.class, args);
	}

}
