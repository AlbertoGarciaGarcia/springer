DROP table IF EXISTS articulos;
DROP table IF EXISTS fabricante;

create table fabricante(
    id Long not null auto_increment primary key,
    nombre varchar(250) null
);
create table articulos(
    id Long not null auto_increment primary key,
    nombre varchar(250),
    precio int,
    fabricante_id int,
    FOREIGN KEY (fabricante_id) REFERENCES fabricante(id)
);

insert into fabricante (id,nombre)values(1,'Bic');
insert into fabricante (id,nombre)values(2,'Valisa');
insert into articulos (nombre, precio,fabricante_id)values('boli', 3,1);
insert into articulos (nombre, precio,fabricante_id)values('vaso', 1,2);