package crudInformatica.Dto;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.util.List;

import javax.persistence.Column;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;


import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name="fabricante")
public class Fabricantes {
	
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre")
	private String nombre;
	
	
	@OneToMany
	@JoinColumn(name="id")
	private List<Articulos> articulos;
	
	
	
	public Fabricantes() {
		
	}
	
	
	public Fabricantes(Long id,String nombre) {
		this.id = id;
		this.nombre = nombre;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Articulos")
	public List<Articulos> getArticulos() {
		return articulos;
	}


	public void setArticulos(List<Articulos> articulos) {
		this.articulos = articulos;
	}
	
	
	@Override
	public String toString() {
		return "Fabricantes [id=" + id + ", nombre=" + nombre +"]";
	}
	
	
}
