package crudInformatica.Service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crudInformatica.Dao.IFabricanteDao;
import crudInformatica.Dto.Fabricantes;
@Service
public class FabricantesServiceImpl implements IFabricantesService {

	//Utilizamos los metodos de la interface IFabricantesDAO, es como si instaciaramos.
		@Autowired
		IFabricanteDao iFabricantesDAO;
		
		@Override
		public List<Fabricantes> listarFabricantess() {
			
			return iFabricantesDAO.findAll();
		}

		@Override
		public Fabricantes guardarFabricantes(Fabricantes Fabricantes) {
			
			return iFabricantesDAO.save(Fabricantes);
		}

		@Override
		public Fabricantes FabricantesXID(Long id) {
			
			return iFabricantesDAO.findById(id).get();
		}

		@Override
		public Fabricantes actualizarFabricantes(Fabricantes Fabricantes) {
			
			return iFabricantesDAO.save(Fabricantes);
		}

		@Override
		public void eliminarFabricantes(Long id) {
			
			iFabricantesDAO.deleteById(id);
			
		}

	
	
	
	
}
