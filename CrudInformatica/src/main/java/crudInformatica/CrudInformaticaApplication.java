package crudInformatica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudInformaticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudInformaticaApplication.class, args);
	}

}
