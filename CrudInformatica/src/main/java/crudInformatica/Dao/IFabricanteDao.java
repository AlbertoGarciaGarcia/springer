package crudInformatica.Dao;

import org.springframework.data.jpa.repository.JpaRepository;

import crudInformatica.Dto.Fabricantes;

public interface IFabricanteDao extends JpaRepository<Fabricantes, Long>{

}
